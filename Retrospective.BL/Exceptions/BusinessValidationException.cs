﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Retrospective.BL.Models;

namespace Retrospective.BL.Exceptions
{
    public class BusinessValidationException : Exception
    {
        public BusinessValidationException(BusinessError businessError)
            : this(new List<BusinessError> {businessError})
        {
        }

        public BusinessValidationException(IEnumerable<BusinessError> errors)
            : base("Business validation failed")
        {
            Errors = new ReadOnlyCollection<BusinessError>(errors.ToArray());
        }

        public ReadOnlyCollection<BusinessError> Errors { get; }
    }
}