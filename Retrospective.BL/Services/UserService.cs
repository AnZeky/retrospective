﻿using System.Collections.Generic;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.BL.Services
{
    public class UserService : IService<UserDto>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IRepository<User> _userRepository;

        public UserService(IUnitOfWorkFactory unitOfWorkFactory, IRepository<User> userRepository,
            IValidator<UserDto> validator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userRepository = userRepository;
            Validator = validator;
        }

        private IValidator<UserDto> Validator { get; }

        public IEnumerable<UserDto> Get(QueryParametersBl queryParameters)
        {
            var users = _userRepository.Get(queryParameters.Map());

            return users.ToDto();
        }

        public UserDto Get(int id)
        {
            var user = _userRepository.Get(id);
            return user.ToDto();
        }

        public UserDto Create(UserDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var user = item.FromDto();
                user = _userRepository.Update(user);

                transaction.Commit();

                item = user.ToDto();
                return item;
            }
        }

        public UserDto Update(UserDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                _userRepository.Update(item.FromDto());

                transaction.Commit();

                return item;
            }
        }

        public void Delete(int id)
        {
            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var user = _userRepository.Get(id);
                _userRepository.Delete(user);

                transaction.Commit();
            }
        }
    }
}