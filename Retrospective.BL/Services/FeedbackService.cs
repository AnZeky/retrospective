﻿using System.Collections.Generic;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.BL.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public FeedbackService(IUnitOfWorkFactory unitOfWorkFactory, IFeedbackRepository feedbackRepository, IValidator<FeedbackDto> validator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _feedbackRepository = feedbackRepository;
            Validator = validator;
        }

        private IValidator<FeedbackDto> Validator { get; }

        public IEnumerable<FeedbackDto> Get(QueryParametersBl queryParameters)
        {
            var items = _feedbackRepository.Get(queryParameters.Map());

            return items.ToDto();
        }

        public FeedbackDto Get(int id)
        {
            var item = _feedbackRepository.Get(id);
            return item.ToDto();
        }

        public void Delete(int id)
        {
            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var item = _feedbackRepository.Get(id);
                _feedbackRepository.Delete(item);

                transaction.Commit();
            }
        }

        public FeedbackDto Create(FeedbackDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var newItem = item.FromDto();
                newItem.Author = new User {Id = item.AuthorId};
                newItem.Column = new Column {Id = item.ColumnId};
                newItem = _feedbackRepository.Update(newItem);

                transaction.Commit();

                item = newItem.ToDto();
                return item;
            }
        }

        public FeedbackDto Update(FeedbackDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var newItem = item.FromDto();
                newItem.Author = new User {Id = item.AuthorId};
                newItem = _feedbackRepository.Update(newItem);

                transaction.Commit();

                item = newItem.ToDto();
                return item;
            }
        }

        public IList<FeedbackDto> GetByRetrospectiveId(int id, QueryParametersBl queryParameters)
        {
            var items = _feedbackRepository.GetByRetrospectiveId(id, queryParameters.Map());

            return items.ToDto();
        }
    }
}