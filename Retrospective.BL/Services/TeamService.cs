﻿using System.Collections.Generic;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.BL.Services
{
    public class TeamService : IService<TeamDto>
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TeamService(IUnitOfWorkFactory unitOfWorkFactory, IRepository<Team> teamRepository,
            IValidator<TeamDto> validator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _teamRepository = teamRepository;
            Validator = validator;
        }

        private IValidator<TeamDto> Validator { get; }

        public IEnumerable<TeamDto> Get(QueryParametersBl queryParameters)
        {
            var teams = _teamRepository.Get(queryParameters.Map());

            return teams.ToDto();
        }

        public TeamDto Get(int id)
        {
            var team = _teamRepository.Get(id);
            return team?.ToDto();
        }

        public TeamDto Create(TeamDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var team = item.FromDto();
                team = _teamRepository.Update(team);

                transaction.Commit();

                item = team.ToDto();
                return item;
            }
        }

        public TeamDto Update(TeamDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                _teamRepository.Update(item.FromDto());

                transaction.Commit();

                return item;
            }
        }

        public void Delete(int id)
        {
            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var team = _teamRepository.Get(id);
                _teamRepository.Delete(team);

                transaction.Commit();
            }
        }
    }
}