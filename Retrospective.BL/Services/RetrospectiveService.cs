﻿using System.Collections.Generic;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.BL.Services
{
    public class RetrospectiveService : IRetrospectiveService
    {
        private readonly IRetrospectiveRepository _retrospectiveRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RetrospectiveService(IUnitOfWorkFactory unitOfWorkFactory, IRetrospectiveRepository retrospectiveRepository, IRepository<Team> _teamRepository, IValidator<RetrospectiveDto> validator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _retrospectiveRepository = retrospectiveRepository;
            this._teamRepository = _teamRepository;
            Validator = validator;
        }

        private IValidator<RetrospectiveDto> Validator { get; }

        public IEnumerable<RetrospectiveDto> Get(QueryParametersBl queryParameters)
        {
            var items = _retrospectiveRepository.Get(queryParameters.Map());

            return items.ToDto();
        }

        public IEnumerable<RetrospectiveDto> GetByTeamId(int id, QueryParametersBl queryParameters)
        {
            var items = _retrospectiveRepository.GetByTeamId(id, queryParameters.Map());

            return items.ToDto();
        }

        public IEnumerable<RetrospectiveDto> GetByUserId(int id, QueryParametersBl queryParameters)
        {
            var items = _retrospectiveRepository.GetByUserId(id, queryParameters.Map());

            return items.ToDto();
        }

        public RetrospectiveDto Get(int id)
        {
            var item = _retrospectiveRepository.Get(id);
            return item.ToDto();
        }

        public void Delete(int id)
        {
            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var item = _retrospectiveRepository.Get(id);
                _retrospectiveRepository.Delete(item);

                transaction.Commit();
            }
        }

        public RetrospectiveDto Create(RetrospectiveDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var newItem = item.FromDto();
                newItem.Team = new Team {Id = item.TeamId};
                newItem = _retrospectiveRepository.Update(newItem);

                transaction.Commit();

                item = newItem.ToDto();
                return item;
            }
        }

        public RetrospectiveDto Update(RetrospectiveDto item)
        {
            Validator.Validate(item);

            using (var transaction = _unitOfWorkFactory.BeginTransaction())
            {
                var newItem = item.FromDto();
                newItem.Team = new Team {Id = item.TeamId};
                newItem = _retrospectiveRepository.Update(newItem);

                transaction.Commit();

                item = newItem.ToDto();
                return item;
            }
        }
    }
}