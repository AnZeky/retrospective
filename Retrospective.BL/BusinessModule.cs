﻿using Autofac;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Models;
using Retrospective.BL.Services;
using Retrospective.BL.Validations;
using Retrospective.DAL;

namespace Retrospective.BL
{
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new DataLayerModule());
            builder.RegisterType<TeamService>().As<IService<TeamDto>>();
            builder.RegisterType<TeamValidator>().As<IValidator<TeamDto>>();

            builder.RegisterType<UserService>().As<IService<UserDto>>();
            builder.RegisterType<UserValidator>().As<IValidator<UserDto>>();

            builder.RegisterType<RetrospectiveService>().As<IRetrospectiveService>().AsSelf();
            builder.RegisterType<RetrospectiveValidator>().As<IValidator<RetrospectiveDto>>();

            builder.RegisterType<FeedbackService>().As<IFeedbackService>();
            builder.RegisterType<FeedbackValidator>().As<IValidator<FeedbackDto>>();
        }
    }
}