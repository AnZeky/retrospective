﻿using System.Collections.Generic;

namespace Retrospective.BL.Models
{
    public class UserDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IEnumerable<int> Teams { get; set; }
        public virtual IEnumerable<int> Feedbacks { get; set; }
    }
}