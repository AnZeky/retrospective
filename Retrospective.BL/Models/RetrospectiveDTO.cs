﻿using System;
using System.Collections.Generic;

namespace Retrospective.BL.Models
{
    public class RetrospectiveDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int TeamId { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual IList<ColumnDto> Columns { get; set; }
    }
}