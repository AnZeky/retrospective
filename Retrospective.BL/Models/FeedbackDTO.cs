﻿namespace Retrospective.BL.Models
{
    public class FeedbackDto
    {
        public virtual int Id { get; set; }
        public virtual string Description { get; set; }
        public virtual int ColumnId { get; set; }
        public virtual int AuthorId { get; set; }
    }
}