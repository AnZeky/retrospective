﻿namespace Retrospective.BL.Models
{
    public class BusinessError
    {
        public BusinessError(string description, string field = null)
        {
            Field = field;
            Description = description;
        }

        public string Field { get; }

        public string Description { get; }
    }
}