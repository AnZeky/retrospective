﻿using System.Collections.Generic;

namespace Retrospective.BL.Models
{
    public class TeamDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IEnumerable<int> Retrospectives { get; set; }
        public virtual IEnumerable<int> Users { get; set; }
    }
}