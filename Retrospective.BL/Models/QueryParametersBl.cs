﻿namespace Retrospective.BL.Models
{
    public class QueryParametersBl
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string Order { get; set; }
    }
}