﻿using System.Collections.Generic;

namespace Retrospective.BL.Models
{
    public class ColumnDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}