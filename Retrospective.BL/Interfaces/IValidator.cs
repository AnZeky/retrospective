﻿namespace Retrospective.BL.Interfaces
{
    public interface IValidator<T>
    {
        void Validate(T item);
    }
}