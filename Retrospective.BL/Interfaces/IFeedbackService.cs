﻿using System.Collections.Generic;
using Retrospective.BL.Models;

namespace Retrospective.BL.Interfaces
{
    public interface IFeedbackService : IService<FeedbackDto>
    {
        IList<FeedbackDto> GetByRetrospectiveId(int id, QueryParametersBl queryParameters);
    }
}