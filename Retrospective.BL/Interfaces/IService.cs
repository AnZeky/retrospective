﻿using System.Collections.Generic;
using Retrospective.BL.Models;

namespace Retrospective.BL.Interfaces
{
    public interface IService<T>
    {
        IEnumerable<T> Get(QueryParametersBl queryParameters);
        T Get(int id);
        T Create(T item);
        T Update(T item);
        void Delete(int id);
    }
}