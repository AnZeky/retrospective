﻿using System.Collections.Generic;
using Retrospective.BL.Models;

namespace Retrospective.BL.Interfaces
{
    public interface IRetrospectiveService : IService<RetrospectiveDto>
    {
        IEnumerable<RetrospectiveDto> GetByTeamId(int id, QueryParametersBl queryParameters);
        IEnumerable<RetrospectiveDto> GetByUserId(int id, QueryParametersBl queryParameters);
    }
}