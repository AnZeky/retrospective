﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Retrospective.BL.Models;
using Retrospective.DAL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class UserMapper
    {
        public static UserDto MapToDto(User item)
        {
            return new UserDto
            {
                Id = item.Id,
                Name = item.Name,
                Feedbacks = item.Feedbacks?.Select(x => x.Id),
                Teams = item.Teams?.Select(x => x.Id)
            };
        }

        public static User MapFromDto(UserDto item)
        {
            return new User
            {
                Id = item.Id,
                Name = item.Name,
                Teams = new List<Team>(item.Teams.Select(x => new Team {Id = x}))
            };
        }

        public static UserDto ToDto(this User user)
        {
            return BaseMapper<UserDto, User>.Map(user, MapToDto);
        }

        public static User FromDto(this UserDto user)
        {
            return BaseMapper<User, UserDto>.Map(user, MapFromDto);
        }

        public static IList<UserDto> ToDto(this IEnumerable<User> users)
        {
            return BaseMapper<UserDto, User>.Map(users, MapToDto);
        }

        public static IList<User> FromDto(this IEnumerable<UserDto> users)
        {
            return BaseMapper<User, UserDto>.Map(users, MapFromDto);
        }
    }
}