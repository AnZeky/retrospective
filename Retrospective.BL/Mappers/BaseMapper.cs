﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    public static class BaseMapper<TFirst, TSecond>
        where TFirst : class, new()
        where TSecond : class, new()
    {
        public static TFirst Map(TSecond element, Func<TSecond, TFirst> stratege)
        {
            var dto = stratege(element);
            return dto;
        }

        public static List<TFirst> Map(IEnumerable<TSecond> elements, Func<TSecond, TFirst> stratege)
        {
            var collections = new List<TFirst>();
            if (elements != null)
                foreach (var element in elements)
                {
                    var dto = Map(element, stratege);
                    if (dto != null)
                        collections.Add(dto);
                }
            return collections;
        }
    }
}