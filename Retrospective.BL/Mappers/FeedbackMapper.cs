﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.BL.Models;
using Retrospective.DAL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class FeedbackMapper
    {
        public static FeedbackDto MapToDto(Feedback item)
        {
            return new FeedbackDto
            {
                Id = item.Id,
                AuthorId = item.Author.Id,
                Description = item.Description,
                ColumnId = item.Column.Id
            };
        }

        public static Feedback MapFromDto(FeedbackDto item)
        {
            return new Feedback
            {
                Id = item.Id,
//                Author = item.Author.FromDto(),
                Description = item.Description,
//                Column = item.Column.FromDto()
            };
        }

        public static FeedbackDto ToDto(this Feedback item)
        {
            return BaseMapper<FeedbackDto, Feedback>.Map(item, MapToDto);
        }

        public static Feedback FromDto(this FeedbackDto item)
        {
            return BaseMapper<Feedback, FeedbackDto>.Map(item, MapFromDto);
        }

        public static IList<FeedbackDto> ToDto(this IEnumerable<Feedback> items)
        {
            return BaseMapper<FeedbackDto, Feedback>.Map(items, MapToDto);
        }

        public static IList<Feedback> FromDto(this IList<FeedbackDto> items)
        {
            return BaseMapper<Feedback, FeedbackDto>.Map(items, MapFromDto);
        }
    }
}