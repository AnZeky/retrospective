﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.BL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class RetrospectiveMapper
    {
        public static RetrospectiveDto MapToDto(DAL.Models.Retrospective item)
        {
            return new RetrospectiveDto
            {
                Id = item.Id,
                Name = item.Name,
                StartTime = item.StartTime,
                Columns = item.Columns.ToDto(),
                TeamId = item.Team.Id
            };
        }

        public static DAL.Models.Retrospective MapFromDto(RetrospectiveDto item)
        {
            return new DAL.Models.Retrospective
            {
                Id = item.Id,
                Name = item.Name,
                StartTime = item.StartTime,
                Columns = item.Columns.FromDto(),
//                Team = item.TeamId.FromDto()
            };
        }

        public static RetrospectiveDto ToDto(this DAL.Models.Retrospective item)
        {
            return BaseMapper<RetrospectiveDto, DAL.Models.Retrospective>.Map(item, MapToDto);
        }

        public static DAL.Models.Retrospective FromDto(this RetrospectiveDto item)
        {
            return BaseMapper<DAL.Models.Retrospective, RetrospectiveDto>.Map(item, MapFromDto);
        }

        public static IList<RetrospectiveDto> ToDto(this IEnumerable<DAL.Models.Retrospective> items)
        {
            return BaseMapper<RetrospectiveDto, DAL.Models.Retrospective>.Map(items, MapToDto);
        }

        public static IList<DAL.Models.Retrospective> FromDto(this IEnumerable<RetrospectiveDto> items)
        {
            return BaseMapper<DAL.Models.Retrospective, RetrospectiveDto>.Map(items, MapFromDto);
        }
    }
}