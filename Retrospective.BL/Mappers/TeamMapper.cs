﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Retrospective.BL.Models;
using Retrospective.DAL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class TeamMapper
    {
        public static TeamDto MapToDto(Team item)
        {
            return new TeamDto
            {
                Id = item.Id,
                Name = item.Name,
                Retrospectives = item.Retrospectives?.Select(x => x.Id),
                Users = item.Users?.Select(x => x.Id)
            };
        }

        public static Team MapFromDto(TeamDto item)
        {
            return new Team
            {
                Id = item.Id,
                Name = item.Name,
                Users = new List<User>(item.Users.Select(x => new User {Id = x}))
            };
        }

        public static TeamDto ToDto(this Team team)
        {
            return BaseMapper<TeamDto, Team>.Map(team, MapToDto);
        }

        public static Team FromDto(this TeamDto team)
        {
            return BaseMapper<Team, TeamDto>.Map(team, MapFromDto);
        }

        public static IList<TeamDto> ToDto(this IEnumerable<Team> teams)
        {
            return BaseMapper<TeamDto, Team>.Map(teams, MapToDto);
        }

        public static IList<Team> FromDto(this IEnumerable<TeamDto> teams)
        {
            return BaseMapper<Team, TeamDto>.Map(teams, MapFromDto);
        }
    }
}