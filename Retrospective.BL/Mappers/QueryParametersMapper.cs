﻿using System.Diagnostics;
using Retrospective.BL.Models;
using Retrospective.DAL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class QueryParametersMapper
    {
        public static QueryParameters MapToQueryParameters(QueryParametersBl @params)
        {
            return new QueryParameters {Limit = @params.Limit, Offset = @params.Offset, Order = @params.Order};
        }

        public static QueryParameters Map(this QueryParametersBl @params)
        {
            return BaseMapper<QueryParameters, QueryParametersBl>.Map(@params, MapToQueryParameters);
        }
    }
}