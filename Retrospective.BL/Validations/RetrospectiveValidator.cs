﻿using System.Collections.Generic;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Models;

namespace Retrospective.BL.Validations
{
    public class RetrospectiveValidator : IValidator<RetrospectiveDto>
    {
        public void Validate(RetrospectiveDto item)
        {
            return;
            var errors = new List<BusinessError>();

            if (string.IsNullOrEmpty(item.Name))
            {
                errors.Add(new BusinessError("Name is empty."));
            }
            else
            {
                if (item.Name.Length < 3)
                    errors.Add(new BusinessError("Teamname too short. Username should be at least 4 characters.",
                        "Name"));

                if (item.Name.Length > 30)
                    errors.Add(new BusinessError("Teamname too short. Username can't exceed 30 characters.", "Name"));
            }

            if (errors.Count != 0)
                throw new BusinessValidationException(errors);
        }
    }
}