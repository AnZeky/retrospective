﻿using System.Collections.Generic;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Models;

namespace Retrospective.BL.Validations
{
    public class FeedbackValidator : IValidator<FeedbackDto>
    {
        public void Validate(FeedbackDto item)
        {
            var errors = new List<BusinessError>();

            if (errors.Count != 0)
                throw new BusinessValidationException(errors);
        }
    }
}