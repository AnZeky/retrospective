﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DAL.Repositories
{
    public class RetrospectiveRepository : IRetrospectiveRepository
    {
        private readonly ISessionProvider _sessionProvider;

        public RetrospectiveRepository(ISessionProvider sessionProvider)
        {
            if (sessionProvider == null)
                throw new ArgumentNullException("sessionProvider");

            _sessionProvider = sessionProvider;
        }


        private ISession Session => _sessionProvider.CurrentSession;

        public IEnumerable<Models.Retrospective> Get(QueryParameters queryParameters)
        {
            var retrospective = Session.Query<Models.Retrospective>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .ToList();
            return retrospective;
        }

        public IEnumerable<Models.Retrospective> GetByTeamId(int id, QueryParameters queryParameters)
        {
            var retrospectives = Session.Query<Models.Retrospective>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .Where(x => x.Team.Id == id)
                .ToList();
            return retrospectives;
        }

        public IEnumerable<Models.Retrospective> GetByUserId(int id, QueryParameters queryParameters)
        {
            var retrospectives = Session.Query<Models.Retrospective>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .Where(x => x.Team.Users.All(y => y.Id == id))
                .ToList();
            return retrospectives;
        }

        public Models.Retrospective Get(int id)
        {
            var retrospective = Session.Get<Models.Retrospective>(id);
            return retrospective;
        }

        public Models.Retrospective Update(Models.Retrospective retrospective)
        {
            Session.SaveOrUpdate(retrospective);
            return retrospective;
        }

        public void Delete(Models.Retrospective retrospective)
        {
            Session.Delete(retrospective);
        }
    }
}