﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ISessionProvider _sessionProvider;

        public TeamRepository(ISessionProvider sessionProvider)
        {
            if (sessionProvider == null)
                throw new ArgumentNullException("sessionProvider");

            _sessionProvider = sessionProvider;
        }

        //private Dictionary<string, Func<Team, object>> orderReg = new Dictionary<string, Func<Team, object>>
        //{
        //    {"name", x => x.Name},
        //    {"id", x => x.Id},
        //    {"name", x => x.Name},
        //    {"name", x => x.Name},
        //};

        private ISession Session => _sessionProvider.CurrentSession;

        public IEnumerable<Team> Get(QueryParameters queryParameters)
        {
            var teams = Session.Query<Team>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .ToList();
            return teams;
        }

        public Team Get(int id)
        {
            var team = Session.Get<Team>(id);
            return team;
        }

        public Team Update(Team team)
        {
            Session.SaveOrUpdate(team);
            return team;
        }

        public void Delete(Team team)
        {
            Session.Delete(team);
        }
    }
}