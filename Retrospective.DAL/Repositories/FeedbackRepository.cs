﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DAL.Repositories
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly ISessionProvider _sessionProvider;

        public FeedbackRepository(ISessionProvider sessionProvider)
        {
            if (sessionProvider == null)
                throw new ArgumentNullException("sessionProvider");

            _sessionProvider = sessionProvider;
        }
        
        private ISession Session => _sessionProvider.CurrentSession;

        public IEnumerable<Feedback> Get(QueryParameters queryParameters)
        {
            var feedbacks = Session.Query<Feedback>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .ToList();
            return feedbacks;
        }

        public IEnumerable<Feedback> GetByRetrospectiveId(int id, QueryParameters queryParameters)
        {
            var feedbacks = Session.Query<Feedback>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .Where(x => x.Column.Retrospective.Id == id)
                .ToList();
            return feedbacks;
        }

        public Feedback Get(int id)
        {
            var feedback = Session.Get<Feedback>(id);
            return feedback;
        }

        public Feedback Update(Feedback feedback)
        {
            Session.SaveOrUpdate(feedback);
            return feedback;
        }

        public void Delete(Feedback feedback)
        {
            Session.Delete(feedback);
        }
    }
}