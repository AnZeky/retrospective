﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ISessionProvider _sessionProvider;

        public UserRepository(ISessionProvider sessionProvider)
        {
            if (sessionProvider == null)
                throw new ArgumentNullException("sessionProvider");

            _sessionProvider = sessionProvider;
        }


        private ISession Session => _sessionProvider.CurrentSession;

        public IEnumerable<User> Get(QueryParameters queryParameters)
        {
            var users = Session.Query<User>()
                .Skip(queryParameters.Offset)
                .Take(queryParameters.Limit)
                .ToList();
            return users;
        }

        public User Get(int id)
        {
            var user = Session.Get<User>(id);
            return user;
        }

        public User Update(User user)
        {
            Session.SaveOrUpdate(user);
            return user;
        }

        public void Delete(User user)
        {
            Session.Delete(user);
        }
    }
}