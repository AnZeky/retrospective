﻿using System.Collections.Generic;

namespace Retrospective.DAL.Models
{
    public class Team
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Retrospective> Retrospectives { get; set; }
        public virtual IList<User> Users { get; set; }
    }
}