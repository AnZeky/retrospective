﻿using System;
using System.Collections.Generic;

namespace Retrospective.DAL.Models
{
    public class Retrospective
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Team Team { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual IList<Column> Columns { get; set; }
    }
}