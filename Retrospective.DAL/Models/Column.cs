﻿using System.Collections.Generic;

namespace Retrospective.DAL.Models
{
    public class Column
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual Retrospective Retrospective { get; set; }
        public virtual IList<Feedback> Feedbacks { get; set; }
    }
}