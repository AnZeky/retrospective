﻿using System.Collections.Generic;

namespace Retrospective.DAL.Models
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Team> Teams { get; set; }
        public virtual IList<Feedback> Feedbacks { get; set; }
    }
}