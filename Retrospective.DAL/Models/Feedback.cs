﻿using System.Collections.Generic;

namespace Retrospective.DAL.Models
{
    public class Feedback
    {
        public virtual int Id { get; set; }
        public virtual string Description { get; set; }
        public virtual Column Column{ get; set; }
        public virtual User Author { get; set; }
    }
}