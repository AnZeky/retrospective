﻿using FluentNHibernate.Mapping;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Mappings
{
    public class ColumnMap : ClassMap<Column>
    {
        public ColumnMap()
        {
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Description);

            References(x => x.Retrospective)
                .Column("RetrospectiveId");

            HasMany(x => x.Feedbacks);
        }
    }
}