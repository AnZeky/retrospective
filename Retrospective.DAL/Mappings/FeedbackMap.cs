﻿using FluentNHibernate.Mapping;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Mappings
{
    public class FeedbackMap : ClassMap<Feedback>
    {
        public FeedbackMap()
        {
            Id(x => x.Id);
            Map(x => x.Description).Not.Nullable();

            References(x => x.Author)
                .Column("AuthorId")
                .Cascade.None();

            References(x => x.Column)
                .Column("ColumnId")
                .Cascade.None();
        }
    }
}