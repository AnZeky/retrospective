﻿using FluentNHibernate.Mapping;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();

            HasMany(x => x.Feedbacks);

            HasManyToMany(x => x.Teams)
                .ParentKeyColumn("UserId")
                .ChildKeyColumn("TeamId")
                .Table("TeamUser");
        }
    }
}