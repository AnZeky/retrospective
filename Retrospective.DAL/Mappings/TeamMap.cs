﻿using FluentNHibernate.Mapping;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Mappings
{
    public class TeamMap : ClassMap<Team>
    {
        public TeamMap()
        {
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();

            HasMany(x => x.Retrospectives);

            HasManyToMany(x => x.Users)
                .ParentKeyColumn("TeamId")
                .ChildKeyColumn("UserId")
                .Table("TeamUser");
        }
    }
}