﻿using FluentNHibernate.Mapping;

namespace Retrospective.DAL.Mappings
{
    public class RetrospectiveMap : ClassMap<Models.Retrospective>
    {
        public RetrospectiveMap()
        {
            Id(x => x.Id);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.StartTime);

            HasMany(x => x.Columns)
                .Cascade.All();

            References(x => x.Team)
                .Column("TeamId");
        }
    }
}