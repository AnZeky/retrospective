﻿using Autofac;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DAL.Repositories;
using Retrospective.DL.Common;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DAL
{
    public class DataLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var connectionString = @"Data Source=ZELINSKYA10;Initial Catalog=Retrospective;Integrated Security=True";

            builder.RegisterModule(new DlCommonModule());
            builder.RegisterType<DbInitializer>().As<IDbInitializer>().WithParameter("connectionString", connectionString);
            builder.RegisterType<TeamRepository>().As<IRepository<Team>>().AsSelf();
            builder.RegisterType<UserRepository>().As<IRepository<User>>().AsSelf();
            builder.RegisterType<FeedbackRepository>().As<IFeedbackRepository>().AsSelf();
            builder.RegisterType<RetrospectiveRepository>().As<IRetrospectiveRepository>().AsSelf();
        }
    }
}