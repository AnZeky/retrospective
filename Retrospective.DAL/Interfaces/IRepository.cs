﻿using System.Collections.Generic;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Get(QueryParameters queryParameters);

        T Get(int id);

        T Update(T item);

        void Delete(T item);
    }
}