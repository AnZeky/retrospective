﻿using System.Collections.Generic;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Interfaces
{
    public interface IRetrospectiveRepository : IRepository<Models.Retrospective>
    {
        IEnumerable<Models.Retrospective> Get(QueryParameters queryParameters);
        IEnumerable<Models.Retrospective> GetByTeamId(int id, QueryParameters queryParameters);
        IEnumerable<Models.Retrospective> GetByUserId(int id, QueryParameters queryParameters);

        Models.Retrospective Get(int id);

        Models.Retrospective Update(Models.Retrospective item);

        void Delete(Models.Retrospective item);
    }
}