﻿using System.Collections.Generic;
using Retrospective.DAL.Models;

namespace Retrospective.DAL.Interfaces
{
    public interface IFeedbackRepository : IRepository<Feedback>
    {
        IEnumerable<Feedback> Get(QueryParameters queryParameters);
        IEnumerable<Feedback> GetByRetrospectiveId(int id, QueryParameters queryParameters);

        Feedback Get(int id);

        Feedback Update(Feedback item);

        void Delete(Feedback item);
    }
}