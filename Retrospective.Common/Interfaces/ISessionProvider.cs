﻿using NHibernate;

namespace Retrospective.DL.Common.Interfaces
{
    public interface ISessionProvider
    {
        ISession CurrentSession { get; }
    }
}