﻿using NHibernate.Cfg;

namespace Retrospective.DL.Common.Interfaces
{
    public interface IDbInitializer
    {
        Configuration GetConfiguration();
    }
}