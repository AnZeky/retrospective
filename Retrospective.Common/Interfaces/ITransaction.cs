﻿using System;

namespace Retrospective.DL.Common.Interfaces
{
    public interface ITransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}