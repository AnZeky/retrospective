﻿namespace Retrospective.DL.Common.Interfaces
{
    public interface IUnitOfWorkFactory
    {
        ITransaction BeginTransaction();
    }
}