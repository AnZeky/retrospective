﻿using Autofac;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DL.Common
{
    public class DlCommonModule : Module
    {
        public static IContainer Container { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterBuildCallback(container => Container = container);
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>();
            builder.RegisterType<SessionProvider>().As<ISessionProvider>();
        }
    }
}