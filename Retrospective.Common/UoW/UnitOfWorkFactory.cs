﻿using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DL.Common
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public ITransaction BeginTransaction()
        {
            return new Transaction(UnitOfWorkConfigurator.GetSession());
        }
    }
}