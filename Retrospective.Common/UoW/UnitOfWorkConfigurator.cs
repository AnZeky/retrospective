﻿using Autofac;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DL.Common
{
    public class UnitOfWorkConfigurator
    {
        private static readonly object LockObject = new object();
        private static Configuration _configuration;
        private static ISessionFactory _sessionFactory;

        public static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    lock (LockObject)
                    {
                        if (_sessionFactory == null)
                        {
                            _sessionFactory = Configuration.BuildSessionFactory();
                        }
                    }
                }

                return _sessionFactory;
            }
        }

        private static Configuration Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    lock (LockObject)
                    {
                        if (_configuration == null)

                        {
                            _configuration = DlCommonModule.Container.Resolve<IDbInitializer>().GetConfiguration();
                        }
                    }
                }

                return _configuration;
            }
        }

        public static ISession GetSession()
        {
            if (CurrentSessionContext.HasBind(SessionFactory))
                return SessionFactory.GetCurrentSession();

//            throw new InvalidOperationException("Database access logic cannot be used, if session not opened.");

            return SessionFactory.OpenSession();
        }
    }
}