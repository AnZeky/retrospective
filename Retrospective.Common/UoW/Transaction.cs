﻿using System;
using NHibernate;
using NHibernate.Context;
using ITransaction = Retrospective.DL.Common.Interfaces.ITransaction;

namespace Retrospective.DL.Common
{
    public class Transaction : ITransaction
    {
        private readonly ISession _session;

        private NHibernate.ITransaction _transaction;

        public Transaction(ISession session)
        {
            if (session == null)
                throw new ArgumentNullException($"bed session");

            CurrentSessionContext.Bind(session);

            _session = session;
            _transaction = session.BeginTransaction();
        }

        public void Dispose()
        {
            Rollback();

            CurrentSessionContext.Unbind(_session.SessionFactory);
            _session.Dispose();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            if (_transaction == null)
                return;

            if (!_transaction.WasCommitted && !_transaction.WasRolledBack)
            {
                _transaction.Rollback();
            }
            _transaction.Dispose();
            _transaction = null;
        }
    }
}