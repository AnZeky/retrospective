﻿using NHibernate;
using Retrospective.DL.Common.Interfaces;

namespace Retrospective.DL.Common
{
    public class SessionProvider : ISessionProvider
    {
        public ISession CurrentSession => UnitOfWorkConfigurator.GetSession();
    }
}