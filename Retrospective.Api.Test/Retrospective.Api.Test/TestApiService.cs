﻿using System.Threading;
using Autofac;
using NUnit.Framework;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Services;
using Retrospective.Api.Services.Retrospective;
using Retrospective.DL.Common.Interfaces;
using TestDL;

namespace Retrospective.Api.Test
{
    [TestFixture]
    public class TestApiService
    {
        [SetUp]
        public void SetUp()
        {
            Container = ContainerFactory.Create();
        }

        private void CreateTeam(object o)
        {
            var teamApiService = Container.Resolve<TeamApiService>();
            var teams = teamApiService.Create(new Team {Name = "Team 0"});
        }

        private IContainer Container { get; set; }

        [Test]
        public void Create100Teams()
        {
            var teamApiService = Container.Resolve<TeamApiService>();

            for (var i = 0; i < 10; i++)
            {
                var s = new Thread(CreateTeam);
                s.Name = i.ToString();
                s.Start(s.Name);
            }
            Thread.Sleep(5000);
            var teams = teamApiService.Get(new QueryParameters {Limit = 100});
        }
    }

    public abstract class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AppServicesModule());
            builder.RegisterType<TestDbInitializer>().As<IDbInitializer>();

            return builder.Build();
        }
    }
}