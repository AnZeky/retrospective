﻿namespace Retrospective.Api.Models.Services
{
    public class QueryParameters
    {
        public int Offset { get; set; } = 0;
        public int Limit { get; set; } = 5;
        public string Order { get; set; } = "ASC";
    }
}