﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Retrospective.Api.Models.Validations;

namespace Retrospective.Api.Models.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(ApiError error)
            : this(new List<ApiError> {error})
        {
        }

        public ValidationException(IEnumerable<ApiError> errors)
            : base("Validation Failed")
        {
            Errors = new ReadOnlyCollection<ApiError>(errors.ToArray());
        }

        public ReadOnlyCollection<ApiError> Errors { get; }
    }
}