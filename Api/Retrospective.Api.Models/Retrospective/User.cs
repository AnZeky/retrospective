﻿using System.Collections.Generic;

namespace Retrospective.Api.Models.Retrospective
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IEnumerable<int> Teams { get; set; }
        public virtual IEnumerable<int> Feedbacks { get; set; }
    }
}