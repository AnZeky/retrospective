﻿using System;
using System.Collections.Generic;

namespace Retrospective.Api.Models.Retrospective
{
    public class Retrospective
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public int TeamId { get; set; }
        public IEnumerable<Column> Columns { get; set; }
    }
}