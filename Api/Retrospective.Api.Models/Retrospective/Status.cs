﻿namespace Retrospective.Api.Models.Retrospective
{
    public enum Status
    {
        Open,
        InProgress,
        Closed
    }
}