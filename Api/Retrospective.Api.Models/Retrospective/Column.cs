﻿using System.Collections.Generic;

namespace Retrospective.Api.Models.Retrospective
{
    public class Column
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}