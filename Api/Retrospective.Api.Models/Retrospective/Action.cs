﻿using System;

namespace Retrospective.Api.Models.Retrospective
{
    public class Action
    {
        public int Id { get; set; }

        public int FeedbackId { get; set; }
        public Feedback Feedback { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DueDate { get; set; }
        public Status Status { get; set; }
    }
}