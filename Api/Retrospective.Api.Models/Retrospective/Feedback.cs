﻿namespace Retrospective.Api.Models.Retrospective
{
    public class Feedback
    {
        public virtual int Id { get; set; }
        public virtual string Description { get; set; }
        public virtual int ColumnId { get; set; }
        public virtual int AuthorId { get; set; }
    }
}