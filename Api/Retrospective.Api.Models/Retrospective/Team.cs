﻿using System.Collections.Generic;

namespace Retrospective.Api.Models.Retrospective
{
    public class Team
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IEnumerable<int> Retrospectives { get; set; }
        public virtual IEnumerable<int> Users { get; set; }
    }
}