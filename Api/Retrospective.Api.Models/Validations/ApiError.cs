﻿using Newtonsoft.Json;

namespace Retrospective.Api.Models.Validations
{
    public class ApiError
    {
        public ApiError(string description, string field = null)
        {
            Field = field;
            Description = description;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }

        public string Description { get; }
    }
}