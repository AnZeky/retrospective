﻿using System;
using System.Collections.Generic;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.Api.Services.Mappers;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;

namespace Retrospective.Api.Services.Retrospective
{
    public class FeedbackApiService : BaseApiService<Feedback>, IApiFeedbackService
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackApiService(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        public override IEnumerable<Feedback> Get(QueryParameters queryParameters)
        {
            var retrospectives = _feedbackService.Get(queryParameters.Map());
            return retrospectives.FromDto();
        }

        public override Feedback Get(int feedbackId)
        {
            var retrospective = _feedbackService.Get(feedbackId);
            return retrospective.FromDto();
        }

        public IEnumerable<Feedback> GetByRetrospectiveId(int retrospectiveId, QueryParameters queryParameters)
        {
            var retrospectives = _feedbackService.GetByRetrospectiveId(retrospectiveId, queryParameters.Map());
            return retrospectives.FromDto();
        }

        public override Feedback Create(Feedback feedback)
        {
            try
            {
                NullValidation(feedback);

                var retrospectiveDto = feedback.ToDto();
                var createdRetrospective = _feedbackService.Create(retrospectiveDto);
                return createdRetrospective.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override Feedback Update(Feedback feedback)
        {
            try
            {
                NullValidation(feedback);

                var retrospectiveDto = feedback.ToDto();
                var updateRetrospective = _feedbackService.Update(retrospectiveDto);
                return updateRetrospective.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override void Delete(int feedbackId)
        {
            try
            {
                _feedbackService.Delete(feedbackId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}