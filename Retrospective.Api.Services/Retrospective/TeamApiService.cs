﻿using System;
using System.Collections.Generic;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.Api.Services.Mappers;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Retrospective
{
    public class TeamApiService : BaseApiService<Team>
    {
        private readonly IService<TeamDto> _teamService;

        public TeamApiService(IService<TeamDto> teamService)
        {
            _teamService = teamService;
        }

        public override IEnumerable<Team> Get(QueryParameters queryParameters)
        {
            var teams = _teamService.Get(queryParameters.Map());
            return teams.FromDto();
        }

        public override Team Get(int teamId)
        {
            var team = _teamService.Get(teamId);
            return team.FromDto();
        }

        public override Team Create(Team team)
        {
            try
            {
                NullValidation(team);

                var teamDto = team.ToDto();
                var createdTeam = _teamService.Create(teamDto);
                return createdTeam.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override Team Update(Team team)
        {
            try
            {
                NullValidation(team);

                var teamDto = team.ToDto();
                var updatedTeam = _teamService.Update(teamDto);
                return updatedTeam.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override void Delete(int teamId)
        {
            try
            {
                _teamService.Delete(teamId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}