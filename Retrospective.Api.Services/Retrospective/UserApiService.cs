﻿using System;
using System.Collections.Generic;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.Api.Services.Mappers;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Retrospective
{
    public class UserApiService : BaseApiService<User>
    {
        private readonly IService<UserDto> _userService;

        public UserApiService(IService<UserDto> userService)
        {
            _userService = userService;
        }

        public override IEnumerable<User> Get(QueryParameters queryParameters)
        {
            var users = _userService.Get(queryParameters.Map());
            return users.FromDto();
        }

        public override User Get(int userId)
        {
            var user = _userService.Get(userId);
            return user.FromDto();
        }

        public override User Create(User user)
        {
            try
            {
                NullValidation(user);

                var userDto = user.ToDto();
                var createdUser = _userService.Create(userDto);
                return createdUser.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override User Update(User user)
        {
            try
            {
                NullValidation(user);

                var userDto = user.ToDto();
                var updatedUser = _userService.Update(userDto);
                return updatedUser.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override void Delete(int userId)
        {
            try
            {
                _userService.Delete(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}