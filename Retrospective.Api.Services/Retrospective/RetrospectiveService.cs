﻿using System;
using System.Collections.Generic;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.Api.Services.Mappers;
using Retrospective.BL.Exceptions;
using Retrospective.BL.Interfaces;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;
using Retrospective.BL.Services;

namespace Retrospective.Api.Services.Retrospective
{
    public class RetrospectiveService : BaseApiService<Models.Retrospective.Retrospective>
    {
        private readonly IRetrospectiveService _retrospectiveService;

        public RetrospectiveService(IRetrospectiveService retrospectiveService)
        {
            _retrospectiveService = retrospectiveService;
        }

        public override IEnumerable<Models.Retrospective.Retrospective> Get(QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.Get(queryParameters.Map());
            return retrospectives.FromDto();
        }

        public IEnumerable<Models.Retrospective.Retrospective> GetByTeamId(int teamId, QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.GetByTeamId(teamId, queryParameters.Map());
            return retrospectives.FromDto();
        }

        public IEnumerable<Models.Retrospective.Retrospective> GetByUserId(int userId, QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.GetByUserId(userId, queryParameters.Map());
            return retrospectives.FromDto();
        }

        public override Models.Retrospective.Retrospective Get(int retrospectiveId)
        {
            var retrospective = _retrospectiveService.Get(retrospectiveId);
            return retrospective.FromDto();
        }

        public override Models.Retrospective.Retrospective Create(Models.Retrospective.Retrospective retrospective)
        {
            try
            {
                NullValidation(retrospective);

                var retrospectiveDto = retrospective.ToDto();
                var createdRetrospective = _retrospectiveService.Create(retrospectiveDto);
                return createdRetrospective.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override Models.Retrospective.Retrospective Update(Models.Retrospective.Retrospective retrospective)
        {
            try
            {
                NullValidation(retrospective);

                var retrospectiveDto = retrospective.ToDto();
                var updateretrospective = _retrospectiveService.Update(retrospectiveDto);
                return updateretrospective.FromDto();
            }
            catch (BusinessValidationException e)
            {
                var errors = new List<ApiError>();
                foreach (var businessError in e.Errors)
                {
                    errors.Add(new ApiError(businessError.Description, businessError.Field));
                }
                throw new ValidationException(errors);
            }
        }

        public override void Delete(int retrospectiveId)
        {
            try
            {
                _retrospectiveService.Delete(retrospectiveId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}