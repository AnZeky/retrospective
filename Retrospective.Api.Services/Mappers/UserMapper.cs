﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.Api.Models.Retrospective;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Mappers
{
    [DebuggerStepThrough]
    internal static class UserMapper
    {
        public static UserDto MapToDto(User item)
        {
            return new UserDto
            {
                Id = item.Id,
                Name = item.Name,
                Feedbacks = item.Feedbacks,
                Teams = item.Teams
            };
        }

        public static User MapFromDto(UserDto item)
        {
            return new User
            {
                Id = item.Id,
                Name = item.Name,
                Feedbacks = item.Feedbacks,
                Teams = item.Teams
            };
        }

        public static UserDto ToDto(this User user)
        {
            return BaseMapper<UserDto, User>.Map(user, MapToDto);
        }

        public static User FromDto(this UserDto user)
        {
            return BaseMapper<User, UserDto>.Map(user, MapFromDto);
        }

        public static IList<UserDto> ToDto(this IEnumerable<User> users)
        {
            return BaseMapper<UserDto, User>.Map(users, MapToDto);
        }

        public static IList<User> FromDto(this IEnumerable<UserDto> users)
        {
            return BaseMapper<User, UserDto>.Map(users, MapFromDto);
        }
    }
}