﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.Api.Models.Retrospective;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Mappers
{
    [DebuggerStepThrough]
    internal static class TeamMapper
    {
        public static TeamDto MapToDto(Team item)
        {
            return new TeamDto
            {
                Id = item.Id,
                Name = item.Name,
                Retrospectives = item.Retrospectives,
                Users = item.Users
            };
        }

        public static Team MapFromDto(TeamDto item)
        {
            return new Team
            {
                Id = item.Id,
                Name = item.Name,
                Retrospectives = item.Retrospectives,
                Users = item.Users
            };
        }

        public static TeamDto ToDto(this Team team)
        {
            return BaseMapper<TeamDto, Team>.Map(team, MapToDto);
        }

        public static Team FromDto(this TeamDto team)
        {
            return BaseMapper<Team, TeamDto>.Map(team, MapFromDto);
        }

        public static IList<TeamDto> ToDto(this IEnumerable<Team> teams)
        {
            return BaseMapper<TeamDto, Team>.Map(teams, MapToDto);
        }

        public static IList<Team> FromDto(this IEnumerable<TeamDto> teams)
        {
            return BaseMapper<Team, TeamDto>.Map(teams, MapFromDto);
        }
    }
}