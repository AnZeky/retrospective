﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.Api.Models.Retrospective;
using Retrospective.BL.Mappers;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Mappers
{
    [DebuggerStepThrough]
    internal static class ColumnMapper
    {
        public static ColumnDto MapToDto(Column item)
        {
            return new ColumnDto
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
            };
        }

        public static Column MapFromDto(ColumnDto item)
        {
            return new Column
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
            };
        }

        public static ColumnDto ToDto(this Column item)
        {
            return BaseMapper<ColumnDto, Column>.Map(item, MapToDto);
        }

        public static Column FromDto(this ColumnDto item)
        {
            return BaseMapper<Column, ColumnDto>.Map(item, MapFromDto);
        }

        public static IList<ColumnDto> ToDto(this IEnumerable<Column> items)
        {
            return BaseMapper<ColumnDto, Column>.Map(items, MapToDto);
        }

        public static IList<Column> FromDto(this IEnumerable<ColumnDto> items)
        {
            return BaseMapper<Column, ColumnDto>.Map(items, MapFromDto);
        }
    }
}