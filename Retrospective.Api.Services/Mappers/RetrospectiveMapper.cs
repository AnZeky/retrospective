﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.Api.Services.Mappers;
using Retrospective.BL.Models;

namespace Retrospective.BL.Mappers
{
    [DebuggerStepThrough]
    internal static class RetrospectiveMapper
    {
        public static RetrospectiveDto MapToDto(Api.Models.Retrospective.Retrospective item)
        {
            return new RetrospectiveDto
            {
                Id = item.Id,
                Name = item.Name,
                StartTime = item.StartTime,
                Columns = item.Columns.ToDto(),
                TeamId = item.TeamId
            };
        }

        public static Api.Models.Retrospective.Retrospective MapFromDto(RetrospectiveDto item)
        {
            return new Api.Models.Retrospective.Retrospective
            {
                Id = item.Id,
                Name = item.Name,
                StartTime = item.StartTime,
                Columns = item.Columns.FromDto(),
                TeamId = item.TeamId
            };
        }

        public static RetrospectiveDto ToDto(this Api.Models.Retrospective.Retrospective item)
        {
            return Api.Services.Mappers.BaseMapper<RetrospectiveDto, Api.Models.Retrospective.Retrospective>.Map(item, MapToDto);
        }

        public static Api.Models.Retrospective.Retrospective FromDto(this RetrospectiveDto item)
        {
            return Api.Services.Mappers.BaseMapper<Api.Models.Retrospective.Retrospective, RetrospectiveDto>.Map(item, MapFromDto);
        }

        public static IList<RetrospectiveDto> ToDto(this IEnumerable<Api.Models.Retrospective.Retrospective> items)
        {
            return Api.Services.Mappers.BaseMapper<RetrospectiveDto, Api.Models.Retrospective.Retrospective>.Map(items, MapToDto);
        }

        public static IList<Api.Models.Retrospective.Retrospective> FromDto(this IEnumerable<RetrospectiveDto> items)
        {
            return Api.Services.Mappers.BaseMapper<Api.Models.Retrospective.Retrospective, RetrospectiveDto>.Map(items, MapFromDto);
        }
    }
}