﻿using System.Collections.Generic;
using System.Diagnostics;
using Retrospective.Api.Models.Retrospective;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Mappers
{
    [DebuggerStepThrough]
    internal static class FeedbackMapper
    {
        public static FeedbackDto MapToDto(Feedback item)
        {
            return new FeedbackDto
            {
                Id = item.Id,
                Description = item.Description,
                AuthorId = item.AuthorId,
                ColumnId = item.ColumnId
            };
        }

        public static Feedback MapFromDto(FeedbackDto item)
        {
            return new Feedback
            {
                Id = item.Id,
                Description = item.Description,
                AuthorId = item.AuthorId,
                ColumnId = item.ColumnId
            };
        }

        public static FeedbackDto ToDto(this Feedback item)
        {
            return BaseMapper<FeedbackDto, Feedback>.Map(item, MapToDto);
        }

        public static Feedback FromDto(this FeedbackDto item)
        {
            return BaseMapper<Feedback, FeedbackDto>.Map(item, MapFromDto);
        }

        public static IList<FeedbackDto> ToDto(this IEnumerable<Feedback> items)
        {
            return BaseMapper<FeedbackDto, Feedback>.Map(items, MapToDto);
        }

        public static IList<Feedback> FromDto(this IEnumerable<FeedbackDto> items)
        {
            return BaseMapper<Feedback, FeedbackDto>.Map(items, MapFromDto);
        }
    }
}