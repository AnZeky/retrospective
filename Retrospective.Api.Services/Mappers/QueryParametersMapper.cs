﻿using System.Diagnostics;
using Retrospective.Api.Models.Services;
using Retrospective.BL.Models;

namespace Retrospective.Api.Services.Mappers
{
    [DebuggerStepThrough]
    internal static class QueryParametersMapper
    {
        public static QueryParametersBl MapToQueryParametersBl(QueryParameters @params)
        {
            return new QueryParametersBl {Limit = @params.Limit, Offset = @params.Offset, Order = @params.Order};
        }

        public static QueryParametersBl Map(this QueryParameters @params)
        {
            return BaseMapper<QueryParametersBl, QueryParameters>.Map(@params, MapToQueryParametersBl);
        }
    }
}