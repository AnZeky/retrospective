﻿using System.Collections.Generic;
using Retrospective.Api.Models.Services;

namespace Retrospective.Api.Services.Infrastructures
{
    public interface IApiService<T>
    {
        IEnumerable<T> Get(QueryParameters queryParameters);
        T Get(int id);
        T Create(T user);
        T Update(T user);
        void Delete(int id);
    }
}