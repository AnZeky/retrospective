﻿using System.Collections.Generic;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;

namespace Retrospective.Api.Services.Infrastructures
{
    public abstract class BaseApiService<T> : IApiService<T>
    {
        public abstract IEnumerable<T> Get(QueryParameters queryParameters);
        public abstract T Get(int id);
        public abstract T Create(T user);
        public abstract T Update(T user);
        public abstract void Delete(int id);

        protected virtual void NullValidation(T item)
        {
            if (item == null)
            {
                throw new ValidationException(new ApiError("Value is null"));
            }
        }
    }
}