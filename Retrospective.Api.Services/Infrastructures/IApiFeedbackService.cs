﻿using System.Collections.Generic;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;

namespace Retrospective.Api.Services.Infrastructures
{
    public interface IApiFeedbackService : IApiService<Feedback>
    {
        IEnumerable<Feedback> GetByRetrospectiveId(int retrospectiveId, QueryParameters queryParameters);
    }
}