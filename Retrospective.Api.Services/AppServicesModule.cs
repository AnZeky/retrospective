﻿using Autofac;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.Api.Services.Retrospective;
using Retrospective.BL;
using Retrospective.BL.Interfaces;

namespace Retrospective.Api.Services
{
    public class AppServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserApiService>().As<IApiService<User>>();
            builder.RegisterType<TeamApiService>().As<IApiService<Team>>();
            builder.RegisterType<FeedbackApiService>().As<IApiFeedbackService>();
            builder.RegisterType<RetrospectiveService>().AsSelf();
            builder.RegisterModule(new BusinessModule());
        }
    }
}