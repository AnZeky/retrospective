﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Retrospective.DAL;
using Retrospective.DAL.Interfaces;
using Retrospective.DAL.Models;
using Retrospective.DL.Common;
using Retrospective.DL.Common.Interfaces;

namespace TestDL
{
    [TestFixture]
    public class UoWFixture
    {
        [SetUp]
        public void SetUp()
        {
            Container = ContainerFactory.Create();

            UnitOfWorkFactory = Container.Resolve<IUnitOfWorkFactory>();
            TeamRepository = Container.Resolve<IRepository<Team>>();
        }

        private IUnitOfWorkFactory UnitOfWorkFactory { get; set; }
        private IContainer Container { get; set; }
        private IRepository<Team> TeamRepository;

        private void TransactionForCreateTeam()
        {
            using (var transaction = UnitOfWorkFactory.BeginTransaction())
            {
                var team = TeamRepository.Update(new Team {Name = "Team", Users = new List<User>()});
                Console.WriteLine(team.Id);
                transaction.Commit();

                if (Thread.CurrentThread.Name == "9")
                {
                    int o = 55;
                    
                }
            }
        }

        [Test]
        public void CanSaveAndLoadStandartNHibernateMethods()
        {
            Team team;
            using (var transaction = UnitOfWorkFactory.BeginTransaction())
            {
                team = TeamRepository.Update(new Team {Name = "Team", Users = new List<User>()});

                transaction.Commit();
            }
            Team newTeam;
            using (var transaction = UnitOfWorkFactory.BeginTransaction())
            {
                newTeam = TeamRepository.Get(team.Id);

                transaction.Commit();
            }
            Assert.IsTrue(team.Name == newTeam.Name);
        }

        [Test]
        public  void CanSaveAndLoadStandartNHibernateMethodsa()
        {
            for (int i = 0; i < 10; i++)
            {
                var s = new Thread(TransactionForCreateTeam);
                s.Name = i.ToString();
                s.Start();
            }
            Assert.True(true);
        }
    }

    public abstract class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new DlCommonModule());
            builder.RegisterModule(new DataLayerModule());
//            builder.RegisterType<TestDbInitializer>().As<IDbInitializer>();

            return builder.Build();
        }
    }
}