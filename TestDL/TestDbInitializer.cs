﻿using System;
using FluentNHibernate;
using NHibernate;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Tool.hbm2ddl;
using Retrospective.DAL.Models;
using Retrospective.DL.Common.Interfaces;
using Environment = NHibernate.Cfg.Environment;

namespace TestDL
{
    public class TestDbInitializer : IDisposable, IDbInitializer
    {
        private static Configuration _configuration;
        private static ISessionFactory _sessionFactory;

        private static readonly object LockObject = new object();
        protected ISession Session;

        public TestDbInitializer()
        {
            var assemblyContainingMapping = typeof(Team).Assembly;
            if (_configuration == null)
            {
                lock (LockObject)
                {
                    if (_configuration == null)
                    {
                        _configuration = new Configuration()
                            .SetProperty(Environment.ReleaseConnections, "on_close")
                            .SetProperty(Environment.Dialect, typeof(SQLiteDialect).AssemblyQualifiedName)
                            .SetProperty(Environment.ConnectionDriver, typeof(SQLite20Driver).AssemblyQualifiedName)
                            .SetProperty(Environment.ConnectionString,
                                "URI=file: mydatabase1.sqlite") //"data source=:memory:")
                            .SetProperty(Environment.CurrentSessionContextClass, "thread_static")
                            .SetProperty(Environment.ProxyFactoryFactoryClass,
                                typeof(DefaultProxyFactoryFactory).AssemblyQualifiedName)
                            .AddMappingsFromAssembly(assemblyContainingMapping)
                            .AddAssembly(assemblyContainingMapping);


                        new SchemaExport(_configuration).Execute(true, true, false);

//                        _sessionFactory = _configuration.BuildSessionFactory();
                    }
                }
            }

//            Session = _sessionFactory.OpenSession();

//            new SchemaExport(_configuration).Execute(true, true, false, session.Connection, Console.Out);
        }

        public Configuration GetConfiguration()
        {
            return _configuration;
        }

        public void Dispose()
        {
            Session.Dispose();
        }
    }
}