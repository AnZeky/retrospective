﻿using NUnit.Framework;
using Retrospective.DAL.Models;

namespace TestDL
{
    [TestFixture]
    public class StandartTestFixture : TestDbInitializer
    {
        //[Test]
        public void CanSaveAndLoadStandartNHibernateMethods()
        {
            object id;

            using (var transaction = Session.BeginTransaction())
            {
                id = Session.Save(new Team
                {
                    Name = "Team"
                });

                transaction.Commit();
            }

            Session.Clear();

            using (var transaction = Session.BeginTransaction())
            {
                var team = Session.Get<Team>(id);

                Assert.AreEqual("Team", team.Name);

                transaction.Commit();
            }
        }
    }
}