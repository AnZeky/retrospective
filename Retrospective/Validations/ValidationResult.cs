﻿using System.Collections.Generic;
using System.Linq;
using Retrospective.Api.Models.Validations;

namespace Retrospective.Validations
{
    public class ValidationResult
    {
        public ValidationResult(IEnumerable<ApiError> errors)
        {
            Description = "Validation Failed";
            Errors = errors.ToList();
        }

        public string Description { get; }

        public List<ApiError> Errors { get; }
    }
}