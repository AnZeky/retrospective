﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HibernatingRhinos.Profiler.Appender.NHibernate;
using Retrospective.Logger;

namespace Retrospective
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            NHibernateProfiler.Initialize();
            AutofacConfig.ConfigureContainer();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            
            GlobalConfiguration.Configuration.MessageHandlers.Add(new PerfomanceMonitoringDelegate());
        }
    }
}