﻿namespace Retrospective.App_Start
{
    public class RoutingName
    {
        public const string User = "User";
        public const string Users = "Users";
        public const string UserRetrospectives = "UserRetrospectives";
        public const string Team = "Team";
        public const string Teams = "Teams";
        public const string TeamRetrospectives = "TeamRetrospectives";
        public const string Retrospective = "Retrospective";
        public const string Retrospectives = "Retrospectives";
        public const string RetrospectiveFeedback = "RetrospectiveFeedback";
        public const string RetrospectiveFeedbacks = "RetrospectiveFeedbacks";
        public const string Feedbacks = "Feedbacks";
    }
}