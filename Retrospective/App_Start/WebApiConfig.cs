﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using Retrospective.App_Start;
using Retrospective.Filters;

namespace Retrospective
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new ValidationFilter());

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                RoutingName.Users,
                "api/users/",
                new {controller = "Users"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.User,
                "api/users/{userId}/",
                new {controller = "User"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.UserRetrospectives,
                "api/users/{userId}/retrospectives",
                new {controller = "Retrospectives", action = "GetByUserId"});


            config.Routes.MapHttpRoute(
                RoutingName.Teams,
                "api/teams/",
                new {controller = "Teams"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.Team,
                "api/teams/{teamId}",
                new {controller = "Team"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.TeamRetrospectives,
                "api/teams/{teamId}/retrospectives",
                new {controller = "Retrospectives"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.Retrospectives,
                "api/retrospectives",
                new {controller = "Retrospectives"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.Retrospective,
                "api/retrospectives/{retrospectiveId}",
                new {controller = "Retrospective"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.Feedbacks,
                "api/feedbacks",
                new {controller = "Feedbacks"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.RetrospectiveFeedbacks,
                "api/Retrospectives/{retrospectiveId}/feedbacks",
                new {controller = "Feedbacks"}
            );

            config.Routes.MapHttpRoute(
                RoutingName.RetrospectiveFeedback,
                "api/retrospectives/{retrospectiveId}/feedbacks/{feedbackId}",
                new {controller = "Feedback"}
            );

            config.Routes.MapHttpRoute(
                "RetrospectiveFeedbackActions",
                "api/retrospectives/{retrospectiveId}/Actions",
                new {controller = "Actions"}
            );

            config.Routes.MapHttpRoute(
                "RetrospectiveFeedbackAction",
                "api/retrospectives/{retrospectiveId}/Feedbacks/{feedbackId}/Action",
                new {controller = "Action"},
                new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
            );

            config.Routes.MapHttpRoute(
                "RetrospectiveAction",
                "api/retrospectives/{retrospectiveId}/Actions/{actionId}",
                new {controller = "Action"}
            );
        }
    }
}