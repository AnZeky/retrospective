﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Validations;

namespace Retrospective.Filters
{
    public class ValidationFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is ValidationException)
            {
                var result = new ValidationResult(((ValidationException) context.Exception).Errors);
                context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
            base.OnException(context);
        }
    }
}