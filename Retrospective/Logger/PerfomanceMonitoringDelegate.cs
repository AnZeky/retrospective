﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Retrospective.Logger
{
    public class PerfomanceMonitoringDelegate : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var watcher = Stopwatch.StartNew();
            var responce = await base.SendAsync(request, cancellationToken);
            watcher.Stop();

            return responce;
        }
    }
}