﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Retrospective.Logger;

[assembly: PreApplicationStartMethod(typeof(PerfomanceHttpModule), "Start")]
namespace Retrospective.Logger
{
    public class PerfomanceHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PostRequestHandlerExecute += ContextOnPostRequestHandlerExecute;
            context.PreRequestHandlerExecute += ContextOnPreRequestHandlerExecute;
        }

        public void Dispose()
        {
        }

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(PerfomanceHttpModule));
        }

        private void ContextOnPreRequestHandlerExecute(object sender, EventArgs eventArgs)
        {
            HttpContext.Current.Items.Add("StartTime", DateTime.Now);
        }

        private void ContextOnPostRequestHandlerExecute(object sender, EventArgs eventArgs)
        {
            var context = HttpContext.Current;
            var now = DateTime.Now;
            var log = new LogData
            {
                RequestStartTime = (DateTime) context.Items["StartTime"],
                RequestRunTime = now - (DateTime) context.Items["StartTime"],
                Controller = context.Request.RequestContext.RouteData.Values["controller"].ToString(),
                Method = context.Request.HttpMethod,
                Path = context.Request.Path
            };

            Log(log);
        }

        private void Log(LogData data)
        {
            Debug.WriteLine(data);

            using (var writer = new StreamWriter("D://data.txt", true))
            {
                writer.WriteLine(data);
            }
        }

        private class LogData
        {
            public DateTime RequestStartTime { get; set; }
            public TimeSpan RequestRunTime { get; set; }
            public string Controller { get; set; }
            public string Method { get; set; }
            public string Path { get; set; }

            public override string ToString()
            {
                var data = new StringBuilder();

                data.Append(RequestStartTime);
                data.Append(" - ");

                data.Append(Method);
                data.Append(" ");

                data.Append(Controller);
                data.Append(" ");

                data.Append(RequestRunTime);
                data.Append(" ");

                data.Append(Path);

                return data.ToString();
            }
        }
    }
}