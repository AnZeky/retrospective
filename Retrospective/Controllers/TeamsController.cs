﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.App_Start;

namespace Retrospective.Controllers
{
    public class TeamsController : BaseApiController
    {
        private readonly IApiService<Team> _teamApiService;

        public TeamsController(IApiService<Team> teamApiService)
        {
            _teamApiService = teamApiService;
        }

        // GET api/Teams
        public HttpResponseMessage Get([FromUri] QueryParameters queryParameters)
        {
            var teams = _teamApiService.Get(queryParameters);

            return ListResult(RoutingName.Teams, teams, new { });
        }

        // POST api/Teams
        public HttpResponseMessage Post([FromBody] Team team)
        {
            NullValidation(team);
            team.Id = 0;
            team.Retrospectives = null;
            var createdTeam = _teamApiService.Create(team);

            return PostResult(RoutingName.Team, createdTeam, new {taemId = createdTeam.Id});
        }

        protected override void NullValidation(object value)
        {
            var item = (Team) value;
            base.NullValidation(item);

            if (item.Users == null || !item.Users.Any())
            {
                throw new ValidationException(new ApiError("Users in Team is null"));
            }
        }
    }
}