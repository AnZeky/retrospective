﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;

namespace Retrospective.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IApiService<User> _userApiService;

        public UserController(IApiService<User> userApiService)
        {
            _userApiService = userApiService;
        }

        // GET api/Users/5
        public HttpResponseMessage Get(int userId)
        {
            var user = _userApiService.Get(userId);
            return GetResult(user);
        }

        // PUT api/Users/5
        public HttpResponseMessage Put(int userId, [FromBody] User user)
        {
            NullValidation(user);
            user.Id = userId;
            user.Feedbacks = null;
            _userApiService.Update(user);

            return PutResult(user);
        }

        // DELETE api/Users/5
        public HttpResponseMessage Delete(int userId)
        {
            _userApiService.Delete(userId);

            return DeleteResult();
        }

        protected override void NullValidation(object value)
        {
            var item = (User) value;
            base.NullValidation(item);

            if (item.Teams == null || !item.Teams.Any())
            {
                throw new ValidationException(new ApiError("Teams in User is null"));
            }
        }
    }
}