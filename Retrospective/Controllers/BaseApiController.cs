﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Validations;
using Retrospective.Helpers;

namespace Retrospective.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        private readonly string LinkHeaderTemplate = "<{0}>; rel=\"{1}\"";

        public Uri Location(string routeName, object routeValues)
        {
            return new Uri(Url.Link(routeName, new HttpRouteValueDictionary(routeValues)));
        }

        protected HttpResponseMessage ListResult<T>(string route, T values, object routeValues)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, values);
            response.Headers.Add("X-Resource-Total", values.ToString());


            var linkBuilder = new PageLinkBuilder(Url, route, routeValues, 0, 20, 100);
            var links = new List<string>();
            if (linkBuilder.FirstPage != null)
                links.Add(string.Format(LinkHeaderTemplate, linkBuilder.FirstPage, "first"));
            if (linkBuilder.PreviousPage != null)
                links.Add(string.Format(LinkHeaderTemplate, linkBuilder.PreviousPage, "previous"));
            if (linkBuilder.NextPage != null)
                links.Add(string.Format(LinkHeaderTemplate, linkBuilder.NextPage, "next"));
            if (linkBuilder.LastPage != null)
                links.Add(string.Format(LinkHeaderTemplate, linkBuilder.LastPage, "last"));

            response.Headers.Add("Link", string.Join(", ", links));

            return response;
        }

        protected HttpResponseMessage PostResult<T>(string routeName, T result, object routeValues)
        {
            var response = Request.CreateResponse(HttpStatusCode.Created, result);
            var lacationUrl = Location(routeName, routeValues);
            response.Headers.Add("Location", lacationUrl.ToString());

            return response;
        }

        protected HttpResponseMessage DeleteResult()
        {
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        protected HttpResponseMessage PutResult<T>(T result)
        {
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        protected HttpResponseMessage GetResult<T>(T result)
        {
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        protected virtual void NullValidation(object item)
        {
            if (item == null)
            {
                throw new ValidationException(new ApiError("Value is null"));
            }
        }
    }
}