﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.App_Start;

namespace Retrospective.Controllers
{
    public class UsersController : BaseApiController
    {
        private readonly IApiService<User> _userApiService;

        public UsersController(IApiService<User> userApiService)
        {
            _userApiService = userApiService;
        }

        // GET api/Users
        public HttpResponseMessage Get([FromUri] QueryParameters queryParameters)
        {
            var users = _userApiService.Get(queryParameters);

            return ListResult(RoutingName.Users, users, new { });
        }

        // POST api/Users
        public HttpResponseMessage Post([FromBody] User user)
        {
            NullValidation(user);
            user.Id = 0;
            user.Feedbacks = null;
            var createdUser = _userApiService.Create(user);

            return PostResult(RoutingName.User, createdUser, new {userId = createdUser.Id});
        }

        protected override void NullValidation(object value)
        {
            var item = (User) value;
            base.NullValidation(item);

            if (item.Teams == null || !item.Teams.Any())
            {
                throw new ValidationException(new ApiError("Teams in User is null"));
            }
        }
    }
}