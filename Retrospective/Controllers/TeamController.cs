﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;

namespace Retrospective.Controllers
{
    public class TeamController : BaseApiController
    {
        private readonly IApiService<Team> _teamApiService;

        public TeamController(IApiService<Team> teamApiService)
        {
            _teamApiService = teamApiService;
        }

        // GET api/Teams/5
        public HttpResponseMessage Get(int teamId)
        {
            var team = _teamApiService.Get(teamId);
            return GetResult(team);
        }

        // PUT api/Teams/5
        public IHttpActionResult Put(int teamId, [FromBody] Team team)
        {
            NullValidation(team);
            team.Id = teamId;
            team.Retrospectives = null;
            _teamApiService.Update(team);

            return Ok();
        }

        // DELETE api/Teams/5
        public HttpResponseMessage Delete(int teamId)
        {
            _teamApiService.Delete(teamId);

            return DeleteResult();
        }

        protected override void NullValidation(object value)
        {
            var item = (Team) value;
            base.NullValidation(item);

            if (item.Users == null || !item.Users.Any())
            {
                throw new ValidationException(new ApiError("Users in Team is null"));
            }
        }
    }
}