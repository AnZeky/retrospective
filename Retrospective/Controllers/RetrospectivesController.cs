﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Retrospective;
using Retrospective.App_Start;

namespace Retrospective.Controllers
{
    public class RetrospectivesController : BaseApiController
    {
        private readonly RetrospectiveService _retrospectiveService;

        public RetrospectivesController(RetrospectiveService retrospectiveService)
        {
            _retrospectiveService = retrospectiveService;
        }

        // GET api/Retrospectives
        public HttpResponseMessage Get([FromUri] QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.Get(queryParameters);


            return ListResult(RoutingName.Retrospective, retrospectives, new { });
        }

        // GET api/Teams/1/Retrospectives
        public HttpResponseMessage GetByTeamId(int teamId, [FromUri] QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.GetByTeamId(teamId, queryParameters);

            return ListResult(RoutingName.TeamRetrospectives, retrospectives, new {teamId}); //"TeamRetrospectives"
        }

        // GET api/Users/1/Retrospectives
        public HttpResponseMessage GetByUserId(int userId, [FromUri] QueryParameters queryParameters)
        {
            var retrospectives = _retrospectiveService.GetByUserId(userId, queryParameters);

            return ListResult(RoutingName.UserRetrospectives, retrospectives, new {userId}); //"UserRetrospectives"
        }


        // POST api/Teams/1/Retrospectives
        public HttpResponseMessage Post(int teamId, [FromBody] Api.Models.Retrospective.Retrospective retrospective)
        {
            NullValidation(retrospective);
            retrospective.Id = 0;
            retrospective.TeamId = teamId;
            retrospective.Columns.ToList().ForEach(x => x.Id = 0);
            var createdRetrospective = _retrospectiveService.Create(retrospective);

            return PostResult(RoutingName.Retrospective, createdRetrospective, new {retrospectiveId = createdRetrospective.Id});
        }

        protected override void NullValidation(object value)
        {
            var item = (Api.Models.Retrospective.Retrospective) value;
            base.NullValidation(item);

            if (item.TeamId == 0)
            {
                throw new ValidationException(new ApiError("TeamId is empty"));
            }

            if (item.Columns == null || !item.Columns.Any())
            {
                throw new ValidationException(new ApiError("Columns in Retrospective is null"));
            }
        }
    }
}