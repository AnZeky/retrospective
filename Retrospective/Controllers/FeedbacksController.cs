﻿using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Services;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;
using Retrospective.App_Start;

namespace Retrospective.Controllers
{
    public class FeedbacksController : BaseApiController
    {
        private readonly IApiFeedbackService _feedbackApiService;

        public FeedbacksController(IApiFeedbackService feedbackApiService)
        {
            _feedbackApiService = feedbackApiService;
        }

        // GET api/Feedbacks
        public HttpResponseMessage Get([FromUri] QueryParameters queryParameters)
        {
            var feedbacks = _feedbackApiService.Get(queryParameters);

            return ListResult(RoutingName.Feedbacks, feedbacks, new { });
        }

        // GET api/Team/1/Retrospectives
        public HttpResponseMessage GetByRetrospectiveId(int retrospectiveId, [FromUri] QueryParameters queryParameters)
        {
            var retrospectives = _feedbackApiService.GetByRetrospectiveId(retrospectiveId, queryParameters);

            return ListResult(RoutingName.RetrospectiveFeedbacks, retrospectives, new {retrospectiveId});
        }

        // POST api/Feedbacks
        public HttpResponseMessage Post(int retrospectiveId, [FromBody] Feedback feedback)
        {
            feedback.Id = 0;
            var createdFeedback = _feedbackApiService.Create(feedback);


            return PostResult(RoutingName.RetrospectiveFeedback, createdFeedback, new {retrospectiveId, feedbackId = createdFeedback.Id});
        }

        protected override void NullValidation(object value)
        {
            var item = (Feedback) value;
            base.NullValidation(item);

            if (item.AuthorId == 0)
            {
                throw new ValidationException(new ApiError("AuthorId is empty"));
            }

            if (item.ColumnId == 0)
            {
                throw new ValidationException(new ApiError("ColumnId is empty"));
            }
        }
    }
}