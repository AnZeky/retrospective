﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Retrospective;

namespace Retrospective.Controllers
{
    public class RetrospectiveController : BaseApiController
    {
        private readonly RetrospectiveService _retrospectiveService;

        public RetrospectiveController(RetrospectiveService retrospectiveService)
        {
            _retrospectiveService = retrospectiveService;
        }

        // GET api/Retrospectives/5
        public HttpResponseMessage Get(int retrospectiveId)
        {
            var retrospective = _retrospectiveService.Get(retrospectiveId);
            return GetResult(retrospective);
        }

        // PUT api/Retrospectives/5
        public HttpResponseMessage Put(int retrospectiveId, [FromBody] Api.Models.Retrospective.Retrospective retrospective)
        {
            NullValidation(retrospective);
            retrospective.Id = retrospectiveId;
            _retrospectiveService.Update(retrospective);

            return PutResult(retrospective);
        }

        // DELETE api/Retrospectives/5
        public HttpResponseMessage Delete(int retrospectiveId)
        {
            _retrospectiveService.Delete(retrospectiveId);

            return DeleteResult();
        }

        protected override void NullValidation(object value)
        {
            var item = (Api.Models.Retrospective.Retrospective) value;
            base.NullValidation(item);

            if (item.TeamId == 0)
            {
                throw new ValidationException(new ApiError("TeamId is empty"));
            }

            if (item.Columns == null || !item.Columns.Any())
            {
                throw new ValidationException(new ApiError("Columns in Retrospective is null"));
            }
        }
    }
}