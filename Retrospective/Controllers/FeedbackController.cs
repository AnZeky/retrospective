﻿using System.Net.Http;
using System.Web.Http;
using Retrospective.Api.Models.Exceptions;
using Retrospective.Api.Models.Retrospective;
using Retrospective.Api.Models.Validations;
using Retrospective.Api.Services.Infrastructures;

namespace Retrospective.Controllers
{
    public class FeedbackController : BaseApiController
    {
        private readonly IApiFeedbackService _feedbackApiService;

        public FeedbackController(IApiFeedbackService feedbackApiService)
        {
            _feedbackApiService = feedbackApiService;
        }

        // GET api/Feedbacks/5
        public HttpResponseMessage Get(int feedbackId)
        {
            var feedback = _feedbackApiService.Get(feedbackId);
            return GetResult(feedback);
        }

        // PUT api/Feedbacks/5
        public HttpResponseMessage Put(int feedbackId, [FromBody] Feedback feedback)
        {
            NullValidation(feedback);
            feedback.Id = feedbackId;
            var updatedFeedback = _feedbackApiService.Update(feedback);

            return PutResult(updatedFeedback);
        }

        // DELETE api/Feedbacks/5
        public HttpResponseMessage Delete(int feedbackId)
        {
            _feedbackApiService.Delete(feedbackId);

            return DeleteResult();
        }

        protected override void NullValidation(object value)
        {
            var item = (Feedback) value;
            base.NullValidation(item);

            if (item.AuthorId == 0)
            {
                throw new ValidationException(new ApiError("AuthorId is empty"));
            }

            if (item.ColumnId == 0)
            {
                throw new ValidationException(new ApiError("ColumnId is empty"));
            }
        }
    }
}