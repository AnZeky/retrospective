﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Retrospective.Helpers
{
    public class PageLinkBuilder
    {
        public Uri FirstPage { get; private set; }
        public Uri LastPage { get; private set; }
        public Uri NextPage { get; private set; }
        public Uri PreviousPage { get; private set; }

        public PageLinkBuilder(UrlHelper urlHelper, string routeName, object routeValues, int offset, int limit, long totalItemsCount)
        {
            // Determine total number of pages
            var pageCount = totalItemsCount > 0
                ? (int)Math.Ceiling(totalItemsCount / (double)limit)
                : 0;

            // Create them page links 
            FirstPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
            {
                {"Offset", 0},
                {"Limit", limit}
            }));
            LastPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
            {
                {"Offset", pageCount},
                {"Limit", limit}
            }));
            if (offset > 1)
            {
                PreviousPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
                {
                    {"Offset", offset - 1},
                    {"Limit", limit}
                }));
            }
            if (offset < pageCount)
            {
                NextPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
                {
                    {"Offset", offset + 1},
                    {"Limit", limit}
                }));
            }
        }
    }
}